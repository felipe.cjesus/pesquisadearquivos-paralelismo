
package pesquisaparalelismo;

/**
 * @author Felipe Costa
 * @author Pedro Ventura
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class LerArquivo {
    
    public static StringBuilder lerArq(String path, String NomeBusca) throws FileNotFoundException, IOException
    {
        StringBuilder result = new StringBuilder();
        
        try 
        {
            int ContLinha = -1;

            File f = new File(path);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));

            String linha = "";

            long tempoInicial = System.currentTimeMillis();
            while (true) 
            {
                if (linha != null) 
                {
                    ContLinha++;
                    
                    if (linha.equals(NomeBusca)) 
                    {
                        System.out.println(
                            "\nRegistro encontrado\nNome: " + NomeBusca   +
                                "\nLinha = "                + ContLinha   +
                                "\nNome do arquivo: "       + f.getName() +
                                "\nCaminho do arquivo: "    + path);

                        long tempoFinal = System.currentTimeMillis();

                        System.out.printf("\nTempo de procura: %.3f ms%n", (tempoFinal - tempoInicial) / 1000d);
                        result.append((String) linha);
                        break;
                    }
                }
                else
                    break;
              
                linha = bufferedReader.readLine();
            }
            bufferedReader.close();
        } 
        catch (IOException e) 
        {
            System.out.println("Erro ao ler o arquivo: " + path);
        }

        return result;

    }	
}
