
package pesquisaparalelismo;

/**
 * @author Felipe Costa
 * @author Pedro Ventura
 */

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Scanner;



public class getFile 
{

    public static void main(String[] args) throws IOException 
    {
        try
        {
        
            boolean find = false;
            LerArquivo l = new LerArquivo();

            Scanner scanner = new Scanner(System.in);

            System.out.print("Digite o caminho do diretório: ");
            String caminho = scanner.nextLine();
            System.out.print("Digite o nome que deseja pesquisar: ");
            String nome = scanner.nextLine();


            //File diretorio = new File("C:/Temp");
            File diretorio = new File(caminho);
            
            long tempoInicial = System.currentTimeMillis();

            File[] listFiles = diretorio.listFiles(new FileFilter() 
            {
                @Override
                public boolean accept(File pathname) 
                {
                    return pathname.getName().endsWith(".txt");     // Apenas arquivos txt                
                }
            });

            for(File f : listFiles) 
            {

                String CaminhoArq = f.toString();
                String NomeBusca = nome;

                StringBuilder ret = l.lerArq(CaminhoArq,NomeBusca);

                if(ret.toString().equals(NomeBusca))
                {       
                    find = true;
                    break;
                }

            }
            
            long tempoFinal = System.currentTimeMillis();
            
            // Validação de tempo total de pesquisa
            System.out.printf("\nTempo Total: %.3f ms%n", (tempoFinal - tempoInicial) / 1000d);
            
            if(find != true)
                System.out.println("Registro não encontrado");
        
        }
        catch(Exception e)
        {
            System.err.println("Erro: " + e.getMessage());
        }
    }
}

